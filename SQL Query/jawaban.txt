Nama: Roosen Gabriel Manurung
Tugas Berlatih SQL
=================================

1. Membuat Database "myshop"
	create database myshop;


2. Membuat Table Di dalam Database "myshop'
	use myshop;

	//Membuat table users
 	create table users(
    		-> id int(10) primary key auto_increment,
    		-> name varchar(255),
    		-> email varchar(255),
    		-> password varchar(255)
    		-> );

	Hasil:

	describe users;
	+----------+--------------+------+-----+---------+----------------+
	| Field    | Type         | Null | Key | Default | Extra          |
	+----------+--------------+------+-----+---------+----------------+
	| id       | int(10)      | NO   | PRI | NULL    | auto_increment |
	| name     | varchar(255) | YES  |     | NULL    |                |
	| email    | varchar(255) | YES  |     | NULL    |                |
	| password | varchar(255) | YES  |     | NULL    |                |
	+----------+--------------+------+-----+---------+----------------+


	//Membuat table categories
	create table categories(
    		-> id int(10) primary key auto_increment,
    		-> name varchar(255)
    		-> );

	Hasil:

	describe categories;
	+-------+--------------+------+-----+---------+----------------+
	| Field | Type         | Null | Key | Default | Extra          |
	+-------+--------------+------+-----+---------+----------------+
	| id    | int(10)      | NO   | PRI | NULL    | auto_increment |
	| name  | varchar(255) | YES  |     | NULL    |                |
	+-------+--------------+------+-----+---------+----------------+


	//Membuat table items
	 create table items(
    		-> id int(10) primary key auto_increment,
    		-> name varchar(255),
	    	-> description varchar(255),
    		-> price int,
    		-> stock int,
    		-> category_id int(10),
    		-> foreign key(category_id) references categories(id)
    		-> );
	
	Hasil:
	describe items;
	+-------------+--------------+------+-----+---------+----------------+
	| Field       | Type         | Null | Key | Default | Extra          |
	+-------------+--------------+------+-----+---------+----------------+
	| id          | int(10)      | NO   | PRI | NULL    | auto_increment |
	| name        | varchar(255) | YES  |     | NULL    |                |
	| description | varchar(255) | YES  |     | NULL    |                |
	| price       | int(11)      | YES  |     | NULL    |                |
	| stock       | int(11)      | YES  |     | NULL    |                |
	| category_id | int(10)      | YES  | MUL | NULL    |                |
	+-------------+--------------+------+-----+---------+----------------+


3). Memasukkan Data pada Table
	
	//Memasukkan data ke table users
	insert into users (name, email, password) values
    		-> ("John Doe" , "john@doe.com" , "john123"),
    		-> ("Jane Doe" , "jane@doe.com" , "jenita123");
	 
		//Menampilkan data pada table users
		select * from users;
		+----+----------+--------------+-----------+
		| id | name     | email        | password  |
		+----+----------+--------------+-----------+
		|  1 | John Doe | john@doe.com | john123   |
		|  2 | Jane Doe | jane@doe.com | jenita123 |
		+----+----------+--------------+-----------+

	//Memasukkan data ke table categories
	insert into categories(name) values
    		-> ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

		//Menampilkan data pada table categories
		select * from categories;
		+----+---------+
		| id | name    |
		+----+---------+
		|  1 | gadget  |
		|  2 | cloth   |
		|  3 | men     |
		|  4 | women   |
		|  5 | branded |
		+----+---------+

	//Memasukkan data ke table items
	insert into items(name, description, price, stock, category_id) values
    		-> ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),
    		-> ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
    		-> ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

		//Menampilkan data pada table items
	 	select * from items;
		+----+-------------+-----------------------------------+---------+-------+-------------+
		| id | name        | description                       | price   | stock | category_id |
		+----+-------------+-----------------------------------+---------+-------+-------------+
		|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
		|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
		|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
		+----+-------------+-----------------------------------+---------+-------+-------------+


4). Mengambil Data dari Database

	a). Mengambil data users, kecuali password.
	 	select id, name, email from users;
		+----+----------+--------------+
		| id | name     | email        |
		+----+----------+--------------+
		|  1 | John Doe | john@doe.com |
		|  2 | Jane Doe | jane@doe.com |
		+----+----------+--------------+

	b). Mengambil data items
		//Data pada items yang memiliki price > 1000000 (satu juta)
			select * from items where price > 1000000 order by price;
			+----+-------------+-----------------------------------+---------+-------+-------------+
			| id | name        | description                       | price   | stock | category_id |
			+----+-------------+-----------------------------------+---------+-------+-------------+
			|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
			|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
			+----+-------------+-----------------------------------+---------+-------+-------------+

		//Mengambil data items dengan kata kunci "watch"
			select * from items where name like "%watch%";
			+----+------------+-----------------------------------+---------+-------+-------------+
			| id | name       | description                       | price   | stock | category_id |
			+----+------------+-----------------------------------+---------+-------+-------------+
			|  3 | IMHO Watch | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
			+----+------------+-----------------------------------+---------+-------+-------------+

	c). Menampilkan data items join dengan categories
		select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items inner join categories on items.category_id = categories.id;
		+-------------+-----------------------------------+---------+-------+-------------+----------+
		| name        | description                       | price   | stock | category_id | kategori |
		+-------------+-----------------------------------+---------+-------+-------------+----------+
		| Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget   |
		| Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth    |
		| IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget   |
		+-------------+-----------------------------------+---------+-------+-------------+----------+

5). Mengubah Data dari Database
	//Mengubah data pada table items dengan name : sumsang b50, dengan price menjadi 2500000
	 update items set price = 2500000 where name = "sumsang b50";

	//Hasilnya
	 select * from items where price = 2500000;
	+----+-------------+-------------------------------+---------+-------+-------------+
	| id | name        | description                   | price   | stock | category_id |
	+----+-------------+-------------------------------+---------+-------+-------------+
	|  1 | Sumsang b50 | hape keren dari merek sumsang | 2500000 |   100 |           1 |
	+----+-------------+-------------------------------+---------+-------+-------------+


